const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController.js")


/*router.post("/add", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
})*/

router.post("/add", auth.admin,( req, res ) => {
    productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId",  (req, res) => {
    console.log(req.params.productId);
    productController.getProduct(req.params.productId).then(result => res.send(result))
})

router.put("/:productId/update", auth.admin, (req, res) => {
    productController.updateProduct(req.params, req.body).then(result => res.send(result))
})


router.put("/:productId/archive", auth.admin, (req,res)=>{
    productController.archivedProduct(req.params, req.body).then(result =>res.send(result))
})

router.put("/:productId/unarchive", auth.admin, (req,res)=>{
    productController.unarchivedProduct(req.params, req.body).then(result =>res.send(result))
})








module.exports = router