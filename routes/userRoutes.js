const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")



router.post("/register",( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", ( req, res ) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

router.put("/admin", auth.admin,(req, res) => {
	userController.setAsAdmin(req.body).then(result => res.send(result))
})


router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )



router.post("/order", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.courseId
	}
	userController.order(data).then(result => res.send(result))
})

router.get("/getorders", (req, res) => {
	userController.getOrders().then(resultFromController => res.send(resultFromController))
})















module.exports = router