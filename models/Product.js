const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Item name is required"]
	},
	description:{
		type: String,
		required: [true, "Item description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price for the item is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	customers:[
		{
			userId:{
				type: String, 
				required: [true, "User ID is required"]
			}
		},
		{
			orderedOn:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema)
