const mongoose = require("mongoose")

const courseSchema = new mongoose.Schema({
	totalAmount:{
		type: Number,
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	customers:[
		{
			userId:{
				type: String, 
				required: [true, "User ID is required"]
			}
		},
		{
			orderedOn:{
				type: Date,
				default: new Date()
			}
		}
	],
	orders: [
	{
		productId:{
			type: String,
			required: [true, "Product ID is required"]
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Ordered"
		}
	}
	]
})

module.exports = mongoose.model("Order", courseSchema)
