const Product = require("../models/Product")
const User = require("../models/User")



//create product


module.exports.addProduct = (reqBody) =>{
    let newProduct = new Product({
        name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
    })
    return newProduct.save().then((saved, error) =>{
        if (error){
            console.log(error)
            return false
        }else{
            return true
        }
    })
}

//retrieve active products
module.exports.getActiveProducts = () => {
    return Product.find( { isActive: true } ).then((result,error) => {
        if (error) {
            console.log(error)
        }else{
            return result
        }
    })
}

//retrieve single product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result
    })
}

//update product
module.exports.updateProduct = ( reqParams, reqBody ) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
        if (error) {
            return false
        } else  {
            return true
        }
    })
}



//archive product
module.exports.archivedProduct=(reqParams, reqBody)=>{
    let updatedProduct = {
        isActive: reqBody.isActive
    }
        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error)=>{
            if (error) {
                return false
            }else{
                return true
            }
    })
}


