const User = require("../models/User.js")
const Product = require("../models/Product.js")
const Order = require("../models/Order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

module.exports.getProfile = (data) => {
	// find the user by the id
	return User.findById( data.userId ).then(result =>{
		// if there is no user found...
		if (result === null) {
			return false
		// if a user is found...
		} else {
			result.password = "";
			return result
		}
	})
}

module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


module.exports.setAsAdmin = async (reqBody) => {
	console.log(1)
	const toAdmin = {isAdmin: true}
	const qwert = await User.findOneAndUpdate( { email: reqBody.email }, toAdmin, {new: true})
	console.log(qwert)
	return 
}



module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push( {productId: data.productId} )
		return user.save().then((user, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({userId: data.userId})
		return product.save().then((product, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	if (isUserUpdated && isProductUpdated){
		return true
	}
	else{
		return false
	}
}

module.exports.getOrders = () => {
    return Order.find( { isActive: true } ).then((result,error) => {
        if (error) {
            console.log(error)
        }else{
            return result
        }
    })
}
